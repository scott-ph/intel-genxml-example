intel-genxml-example
====================

This is a small snippet of the anv code, converted to rust to see
how multi-gen compilation could look. To try it out, checkout a
copy of intel-genxml in this directory and then `cargo build`.
