use std::env;
use std::io;

fn main() -> io::Result<()> {
    let gen = env::current_dir()
        .and_then(|p| {
            p.file_name()
                .map(|n| n.to_owned())
                .ok_or_else(|| io::ErrorKind::Other.into())
        })
        .and_then(|p| {
            p.to_str()
                .map(|s| s.to_owned())
                .ok_or_else(|| io::ErrorKind::Other.into())
        })?;
    println!("cargo:rustc-cfg={}", gen);
    let mut gen_num: f32 = gen[3..].parse().map_err(|_| io::ErrorKind::Other)?;
    if gen_num >= 40.0 {
        gen_num /= 10.0;
    }
    for g in &[4.0, 4.5, 5.0, 6.0, 7.0, 7.5, 8.0, 9.0, 10.0, 11.0, 12.0] {
        if gen_num <= *g {
            println!("cargo:rustc-cfg=gen_lte_{}", g.to_string().replace('.', ""));
        }
        if gen_num >= *g {
            println!("cargo:rustc-cfg=gen_gte_{}", g.to_string().replace('.', ""));
        }
    }
    Ok(())
}
