/*
 * Copyright © 2020 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

use std::{io, io::Cursor};

#[cfg(gen10)]
use intel_genxml::gen10 as gen;
#[cfg(gen11)]
use intel_genxml::gen11 as gen;
#[cfg(gen12)]
use intel_genxml::gen12 as gen;
#[cfg(gen8)]
use intel_genxml::gen8 as gen;
#[cfg(gen9)]
use intel_genxml::gen9 as gen;
use intel_genxml::Serialize;

pub fn init_device_state() -> io::Result<Vec<u8>> {
    let mut batch = Cursor::new(Vec::new());

    gen::PipelineSelect {
        #[cfg(gen_gte_9)]
        mask_bits: 3,
        pipeline_selection: gen::PipelineSelect::_3D,
        ..Default::default()
    }
    .write_to(&mut batch)?;

    #[cfg(gen9)]
    {
        let mut cache_mode_1 = [0u32; 1];
        gen::CacheMode1 {
            float_blend_optimization_enable: true,
            float_blend_optimization_enable_mask: true,
            partial_resolve_disable_in_vc: true,
            partial_resolve_disable_in_vc_mask: true,
            ..Default::default()
        }
        .pack_into(&mut cache_mode_1);

        gen::MiLoadRegisterImm {
            register_offset: gen::CacheMode1::NUM,
            data_dword: cache_mode_1[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }

    gen::_3dStateAaLineParameters::default().write_to(&mut batch)?;

    gen::_3dStateDrawingRectangle {
        clipped_drawing_rectangle_y_min: 0,
        clipped_drawing_rectangle_x_min: 0,
        clipped_drawing_rectangle_y_max: u16::MAX.into(),
        clipped_drawing_rectangle_x_max: u16::MAX.into(),
        drawing_rectangle_origin_y: 0,
        drawing_rectangle_origin_x: 0,
        ..Default::default()
    }
    .write_to(&mut batch)?;

    #[cfg(gen_gte_8)]
    {
        gen::_3dStateWmChromakey::default().write_to(&mut batch)?;

        /* See the Vulkan 1.0 spec Table 24.1 "Standard sample
         * locations" and
         * VkPhysicalDeviceFeatures::standardSampleLocations.
         */
        gen::_3dStateSamplePattern {
            /* XXX: text macro in gen_sample_positions.h makes this
             * more concise */
            _1x_sample0_x_offset: 0.5,
            _1x_sample0_y_offset: 0.5,
            _2x_sample0_x_offset: 0.75,
            _2x_sample0_y_offset: 0.75,
            _2x_sample1_x_offset: 0.25,
            _2x_sample1_y_offset: 0.25,
            _4x_sample0_x_offset: 0.375,
            _4x_sample0_y_offset: 0.125,
            _4x_sample1_x_offset: 0.875,
            _4x_sample1_y_offset: 0.375,
            _4x_sample2_x_offset: 0.125,
            _4x_sample2_y_offset: 0.625,
            _4x_sample3_x_offset: 0.625,
            _4x_sample3_y_offset: 0.875,
            _8x_sample0_x_offset: 0.5625,
            _8x_sample0_y_offset: 0.3125,
            _8x_sample1_x_offset: 0.4375,
            _8x_sample1_y_offset: 0.6875,
            _8x_sample2_x_offset: 0.8125,
            _8x_sample2_y_offset: 0.5625,
            _8x_sample3_x_offset: 0.3125,
            _8x_sample3_y_offset: 0.1875,
            _8x_sample4_x_offset: 0.1875,
            _8x_sample4_y_offset: 0.8125,
            _8x_sample5_x_offset: 0.0625,
            _8x_sample5_y_offset: 0.4375,
            _8x_sample6_x_offset: 0.6875,
            _8x_sample6_y_offset: 0.9375,
            _8x_sample7_x_offset: 0.9375,
            _8x_sample7_y_offset: 0.0625,
            #[cfg(gen_gte_9)]
            _16x_sample0_x_offset: 0.5625,
            #[cfg(gen_gte_9)]
            _16x_sample0_y_offset: 0.5625,
            #[cfg(gen_gte_9)]
            _16x_sample1_x_offset: 0.4375,
            #[cfg(gen_gte_9)]
            _16x_sample1_y_offset: 0.3125,
            #[cfg(gen_gte_9)]
            _16x_sample2_x_offset: 0.3125,
            #[cfg(gen_gte_9)]
            _16x_sample2_y_offset: 0.6250,
            #[cfg(gen_gte_9)]
            _16x_sample3_x_offset: 0.7500,
            #[cfg(gen_gte_9)]
            _16x_sample3_y_offset: 0.4375,
            #[cfg(gen_gte_9)]
            _16x_sample4_x_offset: 0.1875,
            #[cfg(gen_gte_9)]
            _16x_sample4_y_offset: 0.3750,
            #[cfg(gen_gte_9)]
            _16x_sample5_x_offset: 0.6250,
            #[cfg(gen_gte_9)]
            _16x_sample5_y_offset: 0.8125,
            #[cfg(gen_gte_9)]
            _16x_sample6_x_offset: 0.8125,
            #[cfg(gen_gte_9)]
            _16x_sample6_y_offset: 0.6875,
            #[cfg(gen_gte_9)]
            _16x_sample7_x_offset: 0.6875,
            #[cfg(gen_gte_9)]
            _16x_sample7_y_offset: 0.1875,
            #[cfg(gen_gte_9)]
            _16x_sample8_x_offset: 0.3750,
            #[cfg(gen_gte_9)]
            _16x_sample8_y_offset: 0.8750,
            #[cfg(gen_gte_9)]
            _16x_sample9_x_offset: 0.5000,
            #[cfg(gen_gte_9)]
            _16x_sample9_y_offset: 0.0625,
            #[cfg(gen_gte_9)]
            _16x_sample10_x_offset: 0.2500,
            #[cfg(gen_gte_9)]
            _16x_sample10_y_offset: 0.1250,
            #[cfg(gen_gte_9)]
            _16x_sample11_x_offset: 0.1250,
            #[cfg(gen_gte_9)]
            _16x_sample11_y_offset: 0.7500,
            #[cfg(gen_gte_9)]
            _16x_sample12_x_offset: 0.0000,
            #[cfg(gen_gte_9)]
            _16x_sample12_y_offset: 0.5000,
            #[cfg(gen_gte_9)]
            _16x_sample13_x_offset: 0.9375,
            #[cfg(gen_gte_9)]
            _16x_sample13_y_offset: 0.2500,
            #[cfg(gen_gte_9)]
            _16x_sample14_x_offset: 0.8750,
            #[cfg(gen_gte_9)]
            _16x_sample14_y_offset: 0.9375,
            #[cfg(gen_gte_9)]
            _16x_sample15_x_offset: 0.0625,
            #[cfg(gen_gte_9)]
            _16x_sample15_y_offset: 0.0000,
            ..Default::default()
        }
        .write_to(&mut batch)?;

        /* The BDW+ docs describe how to use the 3DSTATE_WM_HZ_OP
         * instruction in the section titled, "Optimized Depth Buffer
         * Clear and/or Stencil Buffer Clear." It mentions that the
         * packet overrides GPU state for the clear operation and needs
         * to be reset to 0s to clear the overrides. Depending on the
         * kernel, we may not get a context with the state for this
         * packet zeroed. Do it ourselves just in case. We've observed
         * this to prevent a number of GPU hangs on ICL.
         */
        gen::_3dStateWmHzOp::default().write_to(&mut batch)?;
    }

    #[cfg(gen11)]
    {
        /* The default behavior of bit 5 "Headerless Message for
         * Pre-emptable Contexts" in SAMPLER MODE register is set to
         * 0, which means headerless sampler messages are not allowed
         * for pre-emptable contexts. Set the bit 5 to 1 to allow
         * them.
         */
        let mut sampler_mode = [0u32; 1];
        gen::SamplerMode {
            headerless_message_for_pre_emptable_contexts: true,
            headerless_message_for_pre_emptable_contexts_mask: true,
        }
        .pack_into(&mut sampler_mode);

        gen::MiLoadRegisterImm {
            register_offset: gen::SamplerMode::NUM,
            data_dword: sampler_mode[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;

        /* Bit 1 "Enabled Texel Offset Precision Fix" must be set in
         * HALF_SLICE_CHICKEN7 register.
         */
        let mut half_slice_chicken7 = [0u32; 1];
        gen::HalfSliceChicken7 {
            enabled_texel_offset_precision_fix: true,
            enabled_texel_offset_precision_fix_mask: true,
        }
        .pack_into(&mut half_slice_chicken7);

        gen::MiLoadRegisterImm {
            register_offset: gen::HalfSliceChicken7::NUM,
            data_dword: half_slice_chicken7[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;

        let mut tccntlreg = [0u32; 1];
        gen::Tccntlreg {
            urb_partial_write_merging_enable: true,
            color_z_partial_write_merging_enable: true,
            l3_data_partial_write_merging_enable: true,
            tc_disable: true,
        }
        .pack_into(&mut tccntlreg);

        gen::MiLoadRegisterImm {
            register_offset: gen::Tccntlreg::NUM,
            data_dword: tccntlreg[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }

    /* genX(emit_slice_hashing_state)(device, &batch); */

    #[cfg(gen_gte_11)]
    {
        /* hardware specification recommends disabling repacking for
         * the compatibility with decompression mechanism in display
         * controller.
         */
        /* if (device->info.disable_ccs_repack) { */
        let mut cache_mode_0 = [0u32; 1];
        gen::CacheMode0 {
            disable_repacking_for_compression: true,
            disable_repacking_for_compression_mask: true,
            ..Default::default()
        }
        .pack_into(&mut cache_mode_0);

        gen::MiLoadRegisterImm {
            register_offset: gen::CacheMode0::NUM,
            data_dword: cache_mode_0[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
        /* } */

        /* an unknown issue is causing vs push constants to become
         * corrupted during object-level preemption. For now, restrict
         * to command buffer level preemption to avoid rendering
         * corruption.
         */
        let mut cs_chicken_1 = [0u32; 1];
        gen::CsChicken1 {
            replay_mode: gen::CsChicken1::MID_CMDBUFFER_PREEMPTION,
            replay_mode_mask: true,
        }
        .pack_into(&mut cs_chicken_1);

        gen::MiLoadRegisterImm {
            register_offset: gen::CsChicken1::NUM,
            data_dword: cs_chicken_1[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }

    #[cfg(gen12)]
    {
        /* uint64_t aux_base_addr = gen_aux_map_get_base(device->aux_map_ctx); */
        /* assert(aux_base_addr % (32 * 1024) == 0); */
        let aux_base_address = 0xffffffff_7fff8000_u64;

        gen::MiLoadRegisterImm {
            register_offset: gen::GfxAuxTableBaseAddr::NUM,
            data_dword: aux_base_address as u32,
            ..Default::default()
        }
        .write_to(&mut batch)?;
        gen::MiLoadRegisterImm {
            register_offset: gen::GfxAuxTableBaseAddr::NUM + 4,
            data_dword: (aux_base_address >> 32) as u32,
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }

    /* Set the "CONSTANT_BUFFER Address Offset Disable" bit, so
     * 3DSTATE_CONSTANT_XS buffer 0 is an absolute address.
     *
     * This is only safe on kernels with context isolation
     * support.
     */
    /* if (device->physical->has_context_isolation) { */
    #[cfg(gen_gte_9)]
    {
        let mut cs_debug_mode2 = [0u32; 1];
        gen::CsDebugMode2 {
            constant_buffer_address_offset_disable: true,
            constant_buffer_address_offset_disable_mask: true,
            ..Default::default()
        }
        .pack_into(&mut cs_debug_mode2);

        gen::MiLoadRegisterImm {
            register_offset: gen::CsDebugMode2::NUM,
            data_dword: cs_debug_mode2[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }
    #[cfg(gen8)]
    {
        let mut instpm = [0u32; 1];
        gen::Instpm {
            constant_buffer_address_offset_disable: true,
            constant_buffer_address_offset_disable_mask: true,
            ..Default::default()
        }
        .pack_into(&mut instpm);

        gen::MiLoadRegisterImm {
            register_offset: gen::Instpm::NUM,
            data_dword: instpm[0],
            ..Default::default()
        }
        .write_to(&mut batch)?;
    }
    /* } */

    /* anv_batch_emit(&batch, GENX(MI_BATCH_BUFFER_END), bbe); */

    /* assert(batch.next <= batch.end); */

    /* return anv_queue_submit_simple_batch(&device->queue, &batch); */

    Ok(batch.into_inner())
}
