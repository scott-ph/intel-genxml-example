use std::io;

pub fn init_device_state(gen: i32) -> io::Result<Vec<u8>> {
    match gen {
        8 => anv_state_init_gen8::init_device_state(),
        9 => anv_state_init_gen9::init_device_state(),
        10 => anv_state_init_gen10::init_device_state(),
        11 => anv_state_init_gen11::init_device_state(),
        12 => anv_state_init_gen12::init_device_state(),
        _ => Err(io::ErrorKind::InvalidInput.into()),
    }
}
