use anv_state_init::*;
use std::io;

fn main() -> io::Result<()> {
    println!("{:x?}", init_device_state(12)?);
    Ok(())
}
